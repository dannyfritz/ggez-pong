use sdl2::keyboard::{KeyboardState, Scancode};

pub struct Control {
    pub scancode: Scancode,
    pub pressed: bool,
}

impl Control {
    fn new(scancode: Scancode) -> Control {
        Control {
            scancode: scancode,
            pressed: false,
        }
    }
    fn update(&mut self, keyboard_state: &KeyboardState) {
        self.pressed = keyboard_state.is_scancode_pressed(self.scancode);
    }
}

pub struct ControlsPlayer {
    pub up: Control,
    pub down: Control,
}

impl ControlsPlayer {
    fn new(up: Scancode, down: Scancode) -> ControlsPlayer {
        ControlsPlayer {
            up: Control::new(up),
            down: Control::new(down),
        }
    }
    fn update(&mut self, keyboard_state: &KeyboardState) {
        self.up.update(&keyboard_state);
        self.down.update(&keyboard_state);
    }
}

pub struct Controls {
    pub players: [ControlsPlayer; 2],
}

impl Controls {
    pub fn new() -> Controls {
        Controls {
            players: [
                ControlsPlayer::new(Scancode::W, Scancode::S),
                ControlsPlayer::new(Scancode::I, Scancode::K),
            ],
        }
    }
    pub fn update(&mut self, keyboard_state: &KeyboardState) {
        for player in self.players.iter_mut() {
            player.update(&keyboard_state);
        }
    }
}
