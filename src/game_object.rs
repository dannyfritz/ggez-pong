use ggez::*;
use nalgebra::geometry::Isometry2;
use ncollide_geometry::shape::Shape2;

pub trait Updatable {
    fn update(&mut self, dt: f64) -> GameResult<()>;
}

pub trait Drawable {
    fn draw(&self, ctx: &mut Context) -> GameResult<()>;
}

pub trait Collidable {
    fn to_shape(&self) -> (Box<Shape2<f32>>, Box<Isometry2<f32>>);
}
