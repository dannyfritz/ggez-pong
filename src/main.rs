extern crate ggez;
extern crate nalgebra;
extern crate ncollide_geometry;
extern crate sdl2;
extern crate time;

mod controls;
mod game_object;
mod pong;
mod state_manager;

use controls::Controls;
use ggez::event::Event::*;
use ggez::*;
use pong::Pong;
use state_manager::StateManager;
use std::env;
use std::path;
use time::precise_time_s;

struct MainState {
    state_manager: Box<StateManager>,
    // let mut t = 0.0;
    dt: f64,
    current_time: f64,
    accumulator: f64,
    // let mut previous_state: Box<State>;
    // let mut current_state: Box<State>;
    controls: Box<Controls>,
}

impl MainState {
    fn new() -> MainState {
        MainState {
            state_manager: Box::new(StateManager::new()),
            // let mut t = 0.0;
            dt: 1.0 / 60.0,
            current_time: precise_time_s(),
            accumulator: 0.0,
            // let mut previous_state: Box<State>;
            // let mut current_state: Box<State>;
            controls: Box::new(Controls::new()),
        }
    }
    fn run(&mut self, ctx: &mut Context) -> GameResult<()> {
        let mut event_pump = ctx.sdl_context
            .event_pump()
            .expect("could not create event_pump");
        'game: loop {
            for event in event_pump.poll_iter() {
                ctx.process_event(&event);
                match event {
                    Quit { .. } => break 'game,
                    _ => {}
                }
            }
            self.controls.update(&event_pump.keyboard_state());
            let new_time = precise_time_s();
            let mut frame_time = new_time - self.current_time;
            if frame_time > 0.25 {
                frame_time = 0.25;
            }
            self.current_time = new_time;
            self.accumulator += frame_time;
            while self.accumulator >= self.dt {
                // previous_state = current_state;
                self.update()?;
                // t += dt;
                self.accumulator -= self.dt;
            }
            //let alpha = accumulator / dt;
            //let state = main_state.clone()
            self.draw(ctx)?;
        }
        Ok(())
    }
    fn update(&mut self) -> GameResult<()> {
        self.state_manager.update(self.dt, &self.controls)?;
        Ok(())
    }
    fn draw(&self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx);
        self.state_manager.draw(ctx)?;
        graphics::present(ctx);
        Ok(())
    }
}

fn main() {
    let main_state = &mut MainState::new();
    main_state.state_manager.push(Box::new(Pong::new()));
    let c = conf::Conf::new();
    let ctx = &mut Context::load_from_conf("pong", "Danny Fritz", c).unwrap();
    if let Ok(manifest_dir) = env::var("CARGO_MANIFEST_DIR") {
        let mut path = path::PathBuf::from(manifest_dir);
        path.push("resources");
        ctx.filesystem.mount(&path, true);
    }
    main_state.run(ctx).unwrap();
}
