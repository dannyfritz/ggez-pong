use game_object::{Collidable, Drawable, Updatable};
use ggez::graphics::{circle, DrawMode, Point2};
use ggez::{Context, GameResult};
use nalgebra;
use nalgebra::geometry::Isometry2;
use nalgebra::Vector2;
use ncollide_geometry::shape::Ball2;
use ncollide_geometry::shape::Shape2;

const BALL_SPEED: f32 = 200.0;

pub struct Ball {
    radius: f32,
    position: Point2,
    velocity: Point2,
}

impl Ball {
    pub fn new() -> Ball {
        Ball {
            radius: 10.0,
            position: Point2::new(400.0, 0.0),
            velocity: Point2::new(BALL_SPEED, 0.0),
        }
    }
    pub fn bounce(&mut self) {
        self.velocity.x = -self.velocity.x;
    }
}

impl Updatable for Ball {
    fn update(&mut self, dt: f64) -> GameResult<()> {
        //TODO:0 Ball should move like pong balls
        self.position.x += self.velocity.x * dt as f32;
        Ok(())
    }
}

impl Drawable for Ball {
    fn draw(&self, ctx: &mut Context) -> GameResult<()> {
        circle(ctx, DrawMode::Fill, self.position, self.radius, 1.0)?;
        Ok(())
    }
}

impl Collidable for Ball {
    fn to_shape(&self) -> (Box<Shape2<f32>>, Box<Isometry2<f32>>) {
        let shape = Box::new(Ball2::new(10.0));
        let pos = Box::new(Isometry2::new(
            Vector2::new(self.position.x, self.position.y),
            nalgebra::zero(),
        ));
        (shape, pos)
    }
}
