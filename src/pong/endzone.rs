use game_object::Collidable;
use ggez::graphics::Point2;
use nalgebra;
use nalgebra::geometry::Isometry2;
use nalgebra::Vector2;
use ncollide_geometry::shape::Cuboid2;
use ncollide_geometry::shape::Shape2;

pub struct Endzone {
    position: Point2,
}

impl Endzone {
    pub fn new(is_left: bool) -> Endzone {
        let mut endzone = Endzone {
            position: Point2::new(-10.0, 0.0),
        };
        if !is_left {
            endzone.position = Point2::new(810.0, 0.0);
        }
        endzone
    }
}

impl Collidable for Endzone {
    fn to_shape(&self) -> (Box<Shape2<f32>>, Box<Isometry2<f32>>) {
        let shape = Box::new(Cuboid2::new(Vector2::new(100.0, 400.0)));
        let pos = Box::new(Isometry2::new(
            Vector2::new(self.position.x, self.position.y),
            nalgebra::zero(),
        ));
        (shape, pos)
    }
}
