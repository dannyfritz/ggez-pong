pub struct IntentPaddle {
    pub move_up: bool,
    pub move_down: bool,
}

impl IntentPaddle {
    pub fn new() -> IntentPaddle {
        IntentPaddle {
            move_up: false,
            move_down: false,
        }
    }
}

// pub struct IntentPong {
//     paddles: [IntentPaddle; 2],
// }
//
// impl IntentPong {
//     pub fn new() -> IntentPong {
//         IntentPong {
//             paddles: [IntentPaddle::new(), IntentPaddle::new()],
//         }
//     }
// }
