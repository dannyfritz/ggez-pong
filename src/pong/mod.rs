mod ball;
mod endzone;
mod intent;
mod paddle;

use self::ball::Ball;
use self::endzone::Endzone;
use self::intent::IntentPaddle;
use self::paddle::Paddle;
use game_object::{Collidable, Drawable, Updatable};
use ggez::{Context, GameResult};
use ncollide_geometry::query;
use state_manager::State;
use Controls;

pub struct Pong {
    endzones: [Endzone; 2],
    paddles: [Paddle; 2],
    balls: Vec<Ball>,
    scores: [usize; 2],
}

impl Pong {
    pub fn new() -> Pong {
        Pong {
            endzones: [Endzone::new(true), Endzone::new(false)],
            paddles: [Paddle::new(true), Paddle::new(false)],
            balls: vec![Ball::new()],
            scores: [0, 0],
        }
    }
}

fn is_colliding(collidable_1: &Collidable, collidable_2: &Collidable) -> bool {
    let (shape_1, pos_1) = collidable_1.to_shape();
    let (shape_2, pos_2) = collidable_2.to_shape();
    match query::proximity(
        pos_1.as_ref(),
        shape_1.as_ref(),
        pos_2.as_ref(),
        shape_2.as_ref(),
        0.0,
    ) {
        query::Proximity::Intersecting => true,
        _ => false,
    }
}

impl State for Pong {
    fn update(&mut self, dt: f64, controls: &Controls) -> GameResult<()> {
        for ball in self.balls.iter_mut() {
            ball.update(dt)?;
        }
        for (i, paddle) in self.paddles.iter_mut().enumerate() {
            paddle.intent(IntentPaddle {
                move_up: controls.players[i].up.pressed,
                move_down: controls.players[i].down.pressed,
            });
            paddle.update(dt)?;
        }
        for ball in self.balls.iter_mut() {
            for (j, endzone) in self.endzones.iter().enumerate() {
                if is_colliding(ball, endzone) {
                    // &self.balls.remove(i);
                    // &self.balls.push(Ball::new());
                    self.scores[j] += 1
                }
            }
            for paddle in self.paddles.iter_mut() {
                if is_colliding(ball, paddle) {
                    ball.bounce();
                }
            }
        }
        println!("{} : {}", self.scores[0], self.scores[1]);
        Ok(())
    }
    fn draw(&self, ctx: &mut Context) -> GameResult<()> {
        for ball in &self.balls {
            ball.draw(ctx)?;
        }
        for paddle in &self.paddles {
            paddle.draw(ctx)?;
        }
        Ok(())
    }
}
