use game_object::{Collidable, Drawable, Updatable};
use ggez::graphics::{rectangle, DrawMode, Point2, Rect};
use ggez::{Context, GameResult};
use nalgebra;
use nalgebra::geometry::Isometry2;
use nalgebra::Vector2;
use ncollide_geometry::shape::Cuboid2;
use ncollide_geometry::shape::Shape2;
use pong::intent::IntentPaddle;

const PADDLE_WIDTH: f32 = 5.0;
const PADDLE_SPEED: f32 = 100.0;

pub struct Paddle {
    size: f32,
    position: Point2,
    intent: IntentPaddle,
}

impl Paddle {
    pub fn new(is_left: bool) -> Paddle {
        let mut paddle = Paddle {
            size: 100.0,
            position: Point2::new(0.0, 0.0),
            intent: IntentPaddle::new(),
        };
        if !is_left {
            paddle.position = Point2::new(700.0, 0.0);
        }
        paddle
    }
    pub fn intent(&mut self, intent: IntentPaddle) {
        self.intent = intent;
    }
}

impl Updatable for Paddle {
    fn update(&mut self, dt: f64) -> GameResult<()> {
        if self.intent.move_up {
            self.position.y -= PADDLE_SPEED * dt as f32;
        }
        if self.intent.move_down {
            self.position.y += PADDLE_SPEED * dt as f32;
        }
        Ok(())
    }
}

impl Drawable for Paddle {
    fn draw(&self, ctx: &mut Context) -> GameResult<()> {
        let x = self.position.x;
        let y = self.position.y;
        rectangle(
            ctx,
            DrawMode::Fill,
            Rect::new(x, y, PADDLE_WIDTH, self.size),
        )?;
        Ok(())
    }
}

impl Collidable for Paddle {
    fn to_shape(&self) -> (Box<Shape2<f32>>, Box<Isometry2<f32>>) {
        let shape = Box::new(Cuboid2::new(Vector2::new(PADDLE_WIDTH / 2.0, 100.0 / 2.0)));
        let pos = Box::new(Isometry2::new(
            Vector2::new(
                self.position.x + PADDLE_WIDTH / 2.0,
                self.position.y + 100.0 / 2.0,
            ),
            nalgebra::zero(),
        ));
        (shape, pos)
    }
}
