use ggez::*;
use Controls;

pub trait State {
    fn update(&mut self, dt: f64, controls: &Controls) -> GameResult<()>;
    fn draw(&self, ctx: &mut Context) -> GameResult<()>;
}

pub struct StateManager {
    states: Vec<Box<State>>,
}

impl StateManager {
    pub fn new() -> StateManager {
        StateManager { states: vec![] }
    }
    pub fn push(&mut self, state: Box<State>) {
        self.states.push(state);
    }
    // pub fn pop(&mut self) -> Option<Box<State>> {
    //     self.states.pop()
    // }
    pub fn update(&mut self, dt: f64, controls: &Controls) -> GameResult<()> {
        for mut state in self.states.iter_mut() {
            state.update(dt, controls)?;
        }
        Ok(())
    }
    pub fn draw(&self, ctx: &mut Context) -> GameResult<()> {
        for mut state in &self.states {
            state.draw(ctx)?;
        }
        Ok(())
    }
}
